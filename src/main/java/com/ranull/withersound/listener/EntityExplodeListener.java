package com.ranull.withersound.listener;

import com.ranull.withersound.WitherSound;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class EntityExplodeListener implements Listener {
    private final WitherSound plugin;

    public EntityExplodeListener(WitherSound plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent event) {
        if (event.getEntityType() == EntityType.WITHER) {
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                if (event.getLocation().getWorld() ==player.getWorld()) {
                    if (event.getEntity().getLocation().distance(player.getLocation())
                            <= plugin.getConfig().getInt("range")) {
                        player.playSound(player.getLocation(), Sound.ENTITY_WITHER_SPAWN, 1F, 1F);
                    }
                }
            }
        }
    }
}
